package array;

public class SortArrayInAscending {
	public static void main(String[] args) 
	{ 
		int[] array = { 4, 1, 5, 2, 3 }; 

		for (int i = 0; i < array.length; i++) { 
			for (int j = i + 1; j < array.length; j++) { 
				if (array[i] > array[j]) { 
					int temp = array[i]; 
					array[i] = array[j]; 
					array[j] = temp; 
				} 
			} 
		} 

		System.out.println("Array elements in Ascending Order:"); 
		for (int i = 0; i < array.length; i++) { 
			System.out.print(array[i] + " "); 
		} 
	} 
}
	 
	       


